// https://docs.cypress.io/api/introduction/api.html

describe("Sample test", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Welcome to the QA Challenge");
  });
});
